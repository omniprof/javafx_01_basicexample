package com.kenfogel.javafx_01_basicexample;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Basic JavaFX program created all in code
 *
 * @author Ken
 */
public class MainApp extends Application {

    private final static Logger LOG = LoggerFactory.getLogger(MainApp.class);

    /**
     * Start the JavaFX application
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {
        // Set window's title
        primaryStage.setTitle("Hello World!");
        // Create a button
        Button btn = new Button();
        btn.setText("Say 'Hello World'");
        btn.setOnAction(event -> System.out.println("Hello World!"));
        // Control added to StackPane is centered in the pane
        StackPane root = new StackPane();
        root.getChildren().add(btn);
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();

        // Event when the window is closed by an external event such as by
        // clicking on Window exit decoration (the X in the title bar).
        // A Platform.exit() is implied but other tasks to carry out on exit
        // can be called here
        primaryStage.setOnCloseRequest(event -> {
            LOG.info("Window close icon pressed");
        });

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
